#!/bin/bash -e
#
# Run Prowler against All AWS Accounts in the AWS Organization
ids_arr=($account_ids)
prowler_s3_bucket=($security_audit_bucket)
prowler_role=($prowler_role)
aws_region=($prowler_aws_region)
prowler_scan_group=($prowler_scan_group)
prowler_output1_format=($prowler_output1_format)
prowler_output2_format=($prowler_output2_format)
log_level=($prowler_log_level)

echo "Commencing Run..."
echo "Current date: $(date)"
run_time=$(date +%FT%T%Z)

#Run Prowler against Accounts in the AWS Organization
echo "AWS Accounts in Organization"
echo "$ids_arr"
Parallel_Accounts="1"

for i in "${!ids_arr[@]}"
do
  test "$(jobs | wc -l)" -ge $PARALLEL_ACCOUNTS && wait || true
 {
  echo "Running Prowler"
  prowler aws --role arn:aws:iam::${ids_arr[${i}]}:role/$prowler_role --compliance $prowler_scan_group -f $aws_region -M $prowler_output1_format $prowler_output2_format -q --security-hub --log-level $log_level --only-logs 
  echo "Prowler scan of AWS Account: ${ids_arr[${i}]} complete"
  }&
done
wait
aws s3 cp ./output/ s3://$prowler_s3_bucket/output/html --recursive --exclude "*" --include "*.html"
echo "Prowler Assessments Completed"
exit

