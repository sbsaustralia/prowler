# SBS Prowler Image #

## Overview ##
This container is a modified version of the [Official Prowler image](https://github.com/toniblyx/prowler)
which is used for performing security audits of SBS's AWS environments.

## Modifications ##
The following modifications have been made from the official source Dockerfile:

* Python is installed
* Pip is used to install prowler
* `start.sh` is copied to the image to support runs
* Changed from `ENTRYPOINT` to `CMD` and pointing at the `start.sh` script

## start.sh ##
`start.sh` is a simple script that has been added to this container to:

* receive & prepare AWS account ids and aliases from environment variables
* enable the scanning of multiple AWS accounts in a single run
* send the failed results to security hub
* upload the results to an S3 bucket in the Security Account

## Expected Environment Variables ##
The following environment variables are expected during normal usage:

* `account_ids` a space delimited string listing all AWS Account IDs
* `account_aliases` a space delimited string listing all the AWS Account
  aliases, in the same order as the `account_ids` variable
* `security_audit_role` an AWS IAM role that exists in each AWS account that
  will be scanned that prowler can assume using the credentials above
* `security_audit_bucket` the name of an S3 bucket that the credentials above
  can be used to which the final reports can be copied

## Usage ##

If you'd like to run this manually, the container can be run as follows:

```
 docker run -ti --rm --name prowler aws --env AWS_ACCESS_KEY_ID \
 --env AWS_SECRET_ACCESS_KEY --env AWS_SESSION_TOKEN --env account_ids \
 --env account_aliases --env security_audit_role --env security_audit_bucket \
 sbsonline/prowler:latest
```

Of course the environment variables above will need to be set in your local
environment or the command above will need to be modified to --env `KEY=VALUE`

## Checks Run ##
By default the `--compliance` flag is set for the prowler command so only the [CIS AWS
Foundations Benchmark](https://d1.awsstatic.com/whitepapers/compliance/AWS_CIS_Foundations_Benchmark.pdf) is run.
