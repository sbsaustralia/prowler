# Pull Amazon Linux image
FROM alpine:3.17

LABEL maintainer="https://github.com/prowler-cloud/prowler"

HEALTHCHECK NONE

# Define Prowler user credentials
ARG USERNAME=prowler
ARG USERID=34000

# Copy Prowler script to path


# Install dependencies
RUN addgroup -g ${USERID} ${USERNAME} && \
    adduser -s /bin/sh -G ${USERNAME} -D -u ${USERID} ${USERNAME}
RUN apk update && \
    apk upgrade
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python && \
    apk --update --no-cache add py3-pip bash jq file
RUN pip3 install --upgrade pip && \
    pip3 install awscli ansi2html boto3 detect-secrets && \
    pip3 install prowler
  
#Clone Prowler repo and add to path
#RUN git clone https://github.com/prowler-cloud/prowler && \

RUN mkdir prowler
COPY run-prowler-reports.sh /prowler 
RUN chmod +x /prowler/run-prowler-reports.sh
RUN chown -R prowler /prowler/

# Run Prowler scan in AWS Organization using the Prowler user
WORKDIR /prowler

USER ${USERNAME}
CMD  ["/prowler/run-prowler-reports.sh"]
