#!/bin/bash
echo "Commencing Run..."
echo "Current date: $(date)"
run_time=$(date +%FT%T%Z)
ids_arr=($account_ids)
aliases_arr=($account_aliases)
echo "Starting Prowler..."
for i in ${!ids_arr[@]}
do
  echo "Running Prowler"
  /prowler/prowler -e -A ${ids_arr[${i}]} -R $security_audit_role | ansi2html -la > /prowler/${run_time}-${aliases_arr[${i}]}-report.html
  echo "Prowler complete, uploading results to S3"
  aws s3 cp /prowler/${run_time}-${aliases_arr[${i}]}-report.html s3://$security_audit_bucket/${aliases_arr[${i}]}/${run_time}-${aliases_arr[${i}]}-report.html --region ap-southeast-2
  echo "Prowler scan of AWS Account: ${ids_arr[${i}]} complete"
done